#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
tvdb.api
~~~~~~~~

This module contains the main interface for making calls to TheTvdb.com API.
"""

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

import requests

from .models import Show
from .exceptions import HTTP404Error, ShowNotFound, UnsupportedLanguage
from . import utils


class Tvdb(object):
    """An interface to TheTVDB.com API.

    :param api_key: A TheTVDB.com API key obtainable from
                    http://thetvdb.com/?tab=apiregister
    :param lang: A two letter (ISO 639-1) language code. Defaults to 'en'.
                 Check :attr:`languages` for a list of supported languages.
    """
    api_series_url = 'http://thetvdb.com/api/%s/series/%d/all/%s.xml'
    api_search_url = 'http://thetvdb.com/api/GetSeries.php?seriesname=%s&language=%s'
    languages = ('cs', 'da', 'de', 'el', 'en', 'es', 'fi', 'fr', 'he',
                 'hr', 'hu', 'it', 'ja', 'ko', 'nl', 'no', 'pl', 'pt',
                 'ru', 'sl', 'sv', 'tr', 'zh')

    def __init__(self, api_key, lang='en'):
        self.api_key = api_key
        self.lang = lang

    @property
    def lang(self):
        return self._lang

    @lang.setter
    def lang(self, value):
        if value not in self.__class__.languages:
            raise UnsupportedLanguage(
                '"%s" is not supported. Check Tvdb.languages '
                'for a list of supported languages.' % value
            )
        self._lang = value

    @staticmethod
    def _get_et(url):
        r = requests.get(url)
        if r.status_code == 404:
            raise HTTP404Error()
        return ET.fromstring(r.content)

    def search(self, name):
        """Search for a show by it's name.

        :param name: A TV show name to search for.
        :return: A list of :class:`Show` objects containing meta-data
                 only (no episodes) or an empty list if show not found.
        """
        et = self._get_et(self.__class__.api_search_url % (name, self.lang))
        shows = [Show(series_node) for series_node in et.findall('Series')]
        return shows

    def get(self, id_or_name):
        """Gets a TV show by it's TVDB id or name. #TODO#

        :param id_or_name: TVDB id (int) or show name (string).
        :return: A :class:`Show` object.
        :raises ShowNotFound: If the specified show was not found.
        """
        if not utils.is_int(id_or_name):
            results = self.search(id_or_name)
            if not results:
                raise ShowNotFound(
                    'The string "%s" did not yield any results.' % id_or_name
                )
            id_or_name = results[0].id

        try:
            et = self._get_et(
                self.__class__.api_series_url % (self.api_key, id_or_name,
                                                 self.lang)
            )
        except HTTP404Error:
            raise ShowNotFound(
                'No TV show with id %d exists.' % id_or_name
            )
        return Show(et)
