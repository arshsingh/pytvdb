#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='pytvdb',
    version='0.1.0',
    description='"A python library for interacting with TheTVDB.com API"',
    long_description=readme + '\n\n' + history,
    author='Arsh Singh',
    author_email='arsh.zingh@gmail.com',
    url='https://github.com/arshsingh/pytvdb',
    packages=[
        'tvdb',
    ],
    package_dir={'tvdb': 'tvdb'},
    include_package_data=True,
    install_requires=[
    ],
    license="MIT",
    zip_safe=False,
    keywords=['pytvdb', 'tvdb', 'thetvdb', 'tv'],
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
    test_suite='tests',
)
