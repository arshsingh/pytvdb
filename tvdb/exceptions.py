#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
tvdb.exceptions
~~~~~~~~~~~~~~~

This module contains pytvdb's exceptions.
"""


class HTTP404Error(Exception):
    """The requested URL returned an HTTP 404 error."""


class UnsupportedLanguage(ValueError):
    """The provided language code is not supprted."""


class ShowNotFound(Exception):
    """The requested TV show could not be found."""
