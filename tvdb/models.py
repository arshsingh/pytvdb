#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
tvdb.models
~~~~~~~~~~~

This module contains the core data abstraction objects used by pytvdb.
"""

from . import utils


@utils.py2_unicode_str
class Episode(object):
    """A class representing an individual episode.

    :param et: An xml.etree.ElementTree.Element object for the episode data
    """
    def __init__(self, et):
        for child in (et.find('Episode') or et):
            if child.text is not None:
                try:
                    txt = (int if child.text.isdigit() else float)(child.text)
                except ValueError:
                    txt = child.text
            else:
                txt = ''

            setattr(self, child.tag.lower(), txt)

    def __repr__(self):
        return '<Episode %d>' % self.id

    def __str__(self):
        return 'S%02dE%02d: %s' % (self.seasonnumber, self.episodenumber,
                                   self.episodename)


@utils.py2_unicode_str
class Show(object):
    """A class representing an individual TV show.

    :param et: An xml.etree.ElementTree.Element object for the show data.
    """
    def __init__(self, et):
        for child in (et.find('Series') or et):
            if child.text is not None:
                try:
                    txt = (int if child.text.isdigit() else float)(child.text)
                except ValueError:
                    txt = child.text
            else:
                txt = ''

            setattr(self, child.tag.lower(), txt)

        self._seasons = []
        self.extra_episodes = []  # Season 0 (Early cuts)

        for ep_node in et.findall('Episode'):
            episode = Episode(ep_node)
            if episode.seasonnumber:
                # #TODO# comment
                diff = episode.seasonnumber - len(self._seasons)
                if diff > 0:
                    self._seasons.extend([[]] * diff)
                self._seasons[episode.seasonnumber - 1].insert(
                    episode.episodenumber - 1, episode)
            else:
                self.extra_episodes.insert(episode.episodenumber - 1, episode)

    def __iter__(self):
        return self._seasons.__iter__()

    def __getitem__(self, v):
        return self._seasons[v]

    def __len__(self):
        return len(self._seasons)

    def __repr__(self):
        return '<Show %d>' % self.id

    def __str__(self):
        return self.seriesname
