#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
pytvdb
~~~~~~

A python library for interacting with TheTVDB.com API.

Basic usage:

   >>> from tvdb import Tvdb
   >>> t = Tvdb('your api key')
   >>> dexter = t.get('dexter')  # or by id : t.get(79349)
   >>> dexter.rating
   '9.3'

Full documentation is available at http://pytvdb.readthedocs.org/

:copyright: (c) 2014 by Arsh Singh.
:license: MIT, see LICENSE for more details.
"""

__author__ = 'Arsh Singh'
__email__ = 'arsh.zingh@gmail.com'
__version__ = '0.1.0'
__license__ = 'MIT'
__copyright__ = 'Copyright 2014 Arsh Singh'

from .api import Tvdb
from .models import Episode, Show
