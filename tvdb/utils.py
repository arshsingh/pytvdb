#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
tvdb.utils
~~~~~~~~~~

This module defines internal utility functions.
"""

import sys


if sys.version_info[0] == 2:
    _str_types = (str, unicode)
    _int_types = (int, long)
else:
    _str_types = (str,)
    _int_types = (int,)


def py2_unicode_str(cls):
    """A class decorator that adds __unicode__ and makes __str__ return
    bytes on python 2.
    """
    if sys.version_info[0] == 2:
        cls.__unicode__ = cls.__str__
        cls.__str__ = lambda self: self.__unicode__().encode('utf-8')
    return cls


def is_str(obj):
    """Returns whether an object is a string."""
    return isinstance(obj, _str_types)


def is_int(obj):
    """Returns whether an object is an integer."""
    return isinstance(obj, _int_types)
