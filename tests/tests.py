#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for pytvdb"""

from os.path import dirname, join, realpath
import sys
import unittest
import xml.etree.ElementTree as ET

from requests import Response

import tvdb
from tvdb.exceptions import ShowNotFound, UnsupportedLanguage


# xml fixtures
XML_DATA = {}


def setUpModule():
    global XML_DATA
    xml_dir = join(dirname(realpath(__file__)), 'xml')
    xml_files = ('search.dexter.en.xml', '79349.en.xml',
                 'search.dexter.ru.xml', '79349.ru.xml',
                 'search.white_collar.en.xml', '108611.en.xml')
    for file_ in xml_files:
        with open(join(xml_dir, file_)) as f:
            XML_DATA[file_[:-4]] = f.read()

    def requests_get_stub(url, **kwargs):
        """A stub for the get function from requests module."""
        series_url = 'http://thetvdb.com/api/fake_api_key/series/%d/all/%s.xml'
        search_url = 'http://thetvdb.com/api/GetSeries.php?seriesname=%s&language=%s'

        xml_data = {
            series_url % (79349, 'en'): XML_DATA['79349.en'],
            search_url % ('dexter', 'en'): XML_DATA['search.dexter.en'],
            series_url % (79349, 'ru'): XML_DATA['79349.ru'],
            search_url % (u'Декстер', 'ru'): XML_DATA['search.dexter.ru'],
            series_url % (108611, 'en'): XML_DATA['108611.en'],
            search_url % ('white collar', 'en'): XML_DATA['search.white_collar.en']
        }

        data = xml_data.get(url)
        r = Response()
        r.status_code = 404 if not data and url[23:27] == 'fake' else 200
        r._content = data or '<dummy-xml></dummy-xml>'
        return r

    # shadow requests.get with a stub
    tvdb.api.requests.get = requests_get_stub


class TvdbTestCase(unittest.TestCase):

    def setUp(self):
        self.t = tvdb.Tvdb(api_key='fake_api_key')

    def test_lang(self):
        """Raise UnsupportedLanguage for a lang not in Tvdb.languages."""
        with self.assertRaises(UnsupportedLanguage):
            self.t.lang = 'elvish'
        with self.assertRaises(UnsupportedLanguage):
            tvdb.Tvdb('fake_api_key', lang='elvish')

    def test_get_by_id(self):
        """Test getting a show by it's TVDB id."""
        self.assertEqual(self.t.get(79349).seriesname, 'Dexter')

    def test_get_by_nonexistent_id(self):
        """Raise ShowNotFound for a non-existent id"""
        with self.assertRaises(ShowNotFound):
            self.t.get(12345678901234567890)

    def test_get_by_name(self):
        """Passing a string to Tvdb.get should search for a show
        and return the first result."""
        self.assertEqual(self.t.get('white collar').id, 108611)

    def test_get_by_nonexistent_name(self):
        """Raise ShowNotFound if Tvdb.get called with a non-existent name."""
        with self.assertRaises(ShowNotFound):
            self.t.get('zippity zoppity zoop zoop')

    def test_get_with_lang(self):
        """Test get method with a different language."""
        self.t.lang = 'ru'
        self.assertEqual(self.t.get(79349).seriesname, u'Декстер')

    def test_search(self):
        """Searching for a show should return a list of all results."""
        results_dexter = self.t.search('dexter')
        results_white_collar = self.t.search('white collar')

        self.assertEqual(results_dexter[0].id, 79349)
        self.assertEqual(len(results_dexter), 1)

        self.assertEqual(results_white_collar[0].id, 108611)
        self.assertEqual(len(results_white_collar), 4)

    def test_search_with_lang(self):
        """Test search method with a different language."""
        self.t.lang = 'ru'
        results = self.t.search(u'Декстер')
        self.assertEqual(results[0].id, 79349)

    def test_search_nonexistent(self):
        """Searching for a non-existent show should return an empty list."""
        self.assertEqual(self.t.search('zippity zoppity zoop zoop'), [])


class ShowTestCase(unittest.TestCase):

    def setUp(self):
        self.shows = {
            'dexter.en': tvdb.Show(ET.fromstring(XML_DATA['79349.en'])),
            'dexter.ru': tvdb.Show(ET.fromstring(XML_DATA['79349.ru']))
        }

    def test_repr(self):
        """repr(show) should return: <Show {id}>."""
        self.assertEqual(repr(self.shows['dexter.en']), '<Show 79349>')
        self.assertEqual(repr(self.shows['dexter.ru']), '<Show 79349>')

    def test_str(self):
        """str(show) should return show.seriesname"""
        self.assertEqual(str(self.shows['dexter.en']), 'Dexter')
        self.assertEqual(str(self.shows['dexter.ru']), 'Декстер')

    @unittest.skipIf(sys.version_info[0] == 3, 'python 2 specific')
    def test_unicode(self):
        """unicode(show) should return show.seriesname"""
        self.assertEqual(unicode(self.shows['dexter.en']), u'Dexter')
        self.assertEqual(unicode(self.shows['dexter.ru']), u'Декстер')

    def test_len(self):
        """len(show) should return the no. of seasons."""
        self.assertEqual(len(self.shows['dexter.en']), 8)

    def test_iter(self):
        """Show object should be iterable over seasons."""
        self.assertEqual(len([s for s in self.shows['dexter.en']]), 8)

    def test_indexing(self):
        """Show object should have zero-based indices for seasons."""
        self.assertEqual(self.shows['dexter.en'][0][11].episodename, 'Born Free')  # S01E12

    def test_indexing_out_of_range(self):
        """Raise IndexError when indexing non-existent seasons."""
        with self.assertRaises(IndexError):
            self.shows['dexter.en'][9]

    def test_numeric_attrs(self):
        """Numeric attributes should be of type int or float, not str."""
        self.assertIsInstance(self.shows['dexter.en'].id, int)
        self.assertIsInstance(self.shows['dexter.en'].rating, float)

    def test_extra_episodes(self):
        """Season 0 should be in show.extra_episodes."""
        self.assertEqual(len(self.shows['dexter.en'].extra_episodes), 29)


class EpisodeTestCase(unittest.TestCase):

    def setUp(self):
        self.shows = {
            'dexter.en': tvdb.Show(ET.fromstring(XML_DATA['79349.en'])),
            'dexter.ru': tvdb.Show(ET.fromstring(XML_DATA['79349.ru']))
        }

    def test_repr(self):
        """repr(episode) should return: <Episode {id}>."""
        self.assertEqual(repr(self.shows['dexter.en'][0][1]), '<Episode 308834>')
        self.assertEqual(repr(self.shows['dexter.ru'][0][1]), '<Episode 308834>')

    def test_str(self):
        """str(episode) should return: S{season}E{episode}: {episode name}"""
        self.assertEqual(str(self.shows['dexter.en'][0][1]),
                         'S01E02: Crocodile')
        self.assertEqual(str(self.shows['dexter.ru'][0][1]),
                         'S01E02: Крокодиловы слёзы')

    @unittest.skipIf(sys.version_info[0] == 3, 'python 2 specific')
    def test_unicode(self):
        """unicode(show) should return show.seriesname"""
        self.assertEqual(unicode(self.shows['dexter.ru'][0][1]),
                         u'S01E02: Крокодиловы слёзы')
        self.assertEqual(unicode(self.shows['dexter.en'][0][1]),
                         u'S01E02: Crocodile')

    def test_numeric_attrs(self):
        """Numeric attributes should be of type int or float, not str."""
        self.assertIsInstance(self.shows['dexter.en'][0][1].id, int)
        self.assertIsInstance(self.shows['dexter.en'][0][1].rating, float)


if __name__ == '__main__':
    unittest.main()
